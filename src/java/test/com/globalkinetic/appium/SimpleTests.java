package com.globalkinetic.appium;


import io.appium.java_client.android.AndroidDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by fundile on 2017/05/03.
 */

public class SimpleTests {
    public AndroidDriver driver;
    public WebElement drawer;
    WebDriverWait wait;
    private static final String HOME = "Home";
    private App app;

    @Before
    public void simpleTest() throws IOException {

        File path = new File("//home/fundile/git/android-UniversalMusicPlayer/mobile/build/outputs/apk/mobile-debug.apk");
        File directory = new File(path, "");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(capabilities.getBrowserName(), "");
        capabilities.setCapability(" platformName", "Android");
        capabilities.setCapability("app", directory.getAbsolutePath());
        capabilities.setCapability("appActivity", ".ui.MusicPlayerActivity");
        capabilities.setCapability("", "");
        capabilities.setCapability("deviceName", "Android");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);


        app = new App();
    }

    @Test
    public void viewExists() {
        Object view = app.getView();
        assertNotNull(view);
    }

    @Test
    public void test() throws IOException {
        boolean flag;
        String titleText = null;

        wait = new WebDriverWait(driver, 50);

        driver.findElement(By.id("toolbar")).getTagName();
        //-------Opens the drawer and tests the username TextView and All music TextView----
        driver.findElementByXPath("//*[@class='android.widget.ImageButton' and @content-desc='Open the main menu']").click();

        String username = driver.findElement(By.id("username")).getTagName();
        drawer = (WebElement) driver.findElements(By.id("design_menu_item_text")).get(1);
        drawer.click();
        String toolbar = driver.findElement(By.id("toolbar")).getTagName();
        wait.until(ExpectedConditions.visibilityOf(driver.findElementByXPath("//*[@class='android.widget.ImageButton' and @content-desc='Open the main menu']"))).click();
        drawer = (WebElement) driver.findElements(By.id("design_menu_item_text")).get(0);
        drawer.click();

        driver.findElement(By.id("title")).click();
        driver.findElement(By.id("title")).click();

        List<WebElement> titleList = driver.findElements(By.id("title"));
        //----Loads the id of all views and get all its TagNames----
        for (WebElement title : titleList) {
            titleText = title.getText();
            if (titleText.equals(HOME)) {
                driver.findElement(By.id("play_eq")).click();
                break;
            }
        }

        String title = driver.findElement(By.id("title")).getTagName();
        String listVIew = driver.findElement(By.id("list_view")).getTagName();
        String desc = driver.findElement(By.id("description")).getTagName();
        String art = driver.findElement(By.id("album_art")).getText();
        driver.findElement(By.id("play_pause")).click();
        driver.findElement(By.id("artist")).click();
        driver.findElement(By.id("prev")).click();
        String next = driver.findElement(By.id("next")).getTagName();
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("play_pause")))).click();
        String endNext = driver.findElement(By.id("endText")).getTagName();

        //---- Comparing tests outcome-----
        assertEquals(next, "android.widget.ImageView");
        assertEquals(endNext, "android.widget.TextView");
        assertEquals(title, "android.widget.TextView");
        assertEquals(toolbar, "android.view.ViewGroup");
        assertEquals(listVIew, "android.widget.ListView");
        assertEquals(desc, "android.widget.TextView");
        assertEquals(titleText, HOME);
        assertEquals(art, "");
        assertEquals(username, "android.widget.TextView");
    }

    @After
    public void stopTest() {
        driver.quit();
    }
}